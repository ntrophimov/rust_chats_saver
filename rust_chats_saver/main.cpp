#include <koicxx/crypt.hpp>
#include <koicxx/time.hpp>

#include <boost/algorithm/string/predicate.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/format.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/scope_exit.hpp>

#define HAVE_REMOTE
#include <pcap.h>

#include <algorithm>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#pragma comment(lib, "wpcap.lib")

#define MAX_IP_PACKET_SIZE     65536
#define PACKET_READ_TIMEOUT    20

struct ethernet_header
{
  uint8_t  dest[6];                          // Destination MAC Address
  uint8_t  source[6];                        // Source MAC Address
  uint16_t ether_type;                       // EtherType
};

struct ipv4_header
{
  uint8_t   internet_header_length     : 4;  // Internet Header Length
  uint8_t   version                    : 4;  // Version
  uint8_t   ecn                        : 2;  // Explicit Congestion Notification (ECN)
  uint8_t   dscp                       : 6;  // Differentiated Services Code Point (DSCP)
  uint16_t  total_length;                    // Total Length
  uint16_t  identification;                  // Identification
  uint8_t   fragment_offset_first_part : 5;  // Fragment Offset

  uint8_t   mf                         : 1;  // More Fragments (MF)
  uint8_t   df                         : 1;  // Don't Fragment (DF)
  uint8_t   reserved                   : 1;  // Reserved; must be zero

  uint8_t   fragment_offset_second_part;     // Fragment Offset

  uint8_t   ttl;                             // Time To Live
  uint8_t   protocol;                        // Protocol
  uint16_t  header_checksum;                 // Header Checksum
  uint32_t  src_ip_addr;                     // Source IP Address
  uint32_t  dest_ip_addr;                    // Destination IP Address
};

struct udp_header
{
  uint16_t source_port;                      // Source port
  uint16_t dest_port;                        // Destination port
  uint16_t length;                           // Packet length
  uint16_t checksum;                         // Checksum
};

struct data_payload
{
  data_payload(const u_char* data, const int data_size)
    : data(data)
    , data_size(data_size) {}

  const u_char* data;
  const int data_size;
};

struct message
{
  std::string sender;
  std::string msg;
};

struct sent_packet
{
  sent_packet(const std::string& payload_hash, std::time_t t)
    : payload_hash(payload_hash)
    , t(t) {}

  std::string payload_hash;
  std::time_t t;
};

std::string g_rust_server_ip_address;
std::vector<sent_packet> g_sent_packets;

std::string get_hex_representation_of_data(const u_char* buffer, int buffer_size)
{
  std::ostringstream osstr;

  for (int i = 0; i < buffer_size; ++i)
  {
    if (i)
    {
      osstr << ' ';
    }
    osstr << (boost::format("%.2x") % (int)buffer[i]).str();
  }

  return osstr.str();
}

data_payload get_udp_data_payload(const u_char* buffer, int buffer_size)
{
  ipv4_header* ip_hdr = (ipv4_header*)(buffer + sizeof(ethernet_header));
  int ip_hdr_len = ip_hdr->internet_header_length * 4;

  udp_header* udp_hdr = (udp_header*)(buffer + ip_hdr_len + sizeof(ethernet_header));

  const u_char* data = buffer + sizeof(ethernet_header) + ip_hdr_len + sizeof(udp_header);
  const int data_size = buffer_size - sizeof(ethernet_header) - ip_hdr_len - sizeof(udp_header);

  data_payload res(data, data_size);

  return res;
}

bool is_chat_add_packet(const u_char* buffer, int buffer_size)
{
  const data_payload packet_data_payload = get_udp_data_payload(buffer, buffer_size);
  return boost::contains((char*)packet_data_payload.data, "chat.add");
}

message get_message(const u_char* buffer, int buffer_size)
{
  message res;

  const data_payload packet_data_payload = get_udp_data_payload(buffer, buffer_size);
  const std::string buffer_str((char*)packet_data_payload.data);

  const auto chat_add_begin = buffer_str.find("chat.add");

  const auto sender_begin = buffer_str.find("\"", chat_add_begin);
  const auto sender_end = buffer_str.find("\"", sender_begin + 1);
  const auto sender_length = sender_end - sender_begin + 1;
  const std::string sender(buffer_str.substr(sender_begin, sender_length));

  const auto msg_begin = buffer_str.find("\"", sender_end + 1);
  const auto msg_end = buffer_str.find("\"", msg_begin + 1);
  const auto msg_length = msg_end - msg_begin + 1;
  const std::string msg(buffer_str.substr(msg_begin, msg_length));

  res.sender = sender;
  res.msg = msg;

  return res;
}

std::string get_packet_data_payload_hash_text(const u_char* buffer, int buffer_size)
{
  const data_payload packet_data_payload = get_udp_data_payload(buffer, buffer_size);
  const std::string& packet_data_payload_hex(get_hex_representation_of_data(packet_data_payload.data, packet_data_payload.data_size));
  const std::string& packet_data_payload_hash_text(
    koicxx::crypt::get_hash_text(packet_data_payload_hex.c_str(), packet_data_payload_hex.size(), koicxx::crypt::hash_type::MD5)
  );

  return packet_data_payload_hash_text;
}

void add_packet_to_sent_packets(const u_char* buffer, int buffer_size)
{
  const std::string& packet_data_payload_hash_text(
    get_packet_data_payload_hash_text(buffer, buffer_size)
  );

  g_sent_packets.emplace_back(packet_data_payload_hash_text, std::time(0));
}

bool is_duplicate_packet(const u_char* buffer, int buffer_size)
{
  const std::string& packet_data_payload_hash_text(
    get_packet_data_payload_hash_text(buffer, buffer_size)
  );

  const auto& itr = std::find_if(
    g_sent_packets.begin()
    , g_sent_packets.end()
    , [&packet_data_payload_hash_text](const sent_packet& msg)
    {
      return msg.payload_hash == packet_data_payload_hash_text;
    }
  );
  if (itr != g_sent_packets.end())
  {
    return true;
  }

  return false;
}

void remove_old_sent_packets()
{
  g_sent_packets.erase(
    std::remove_if(
      std::begin(g_sent_packets)
      , std::end(g_sent_packets)
      , [](const sent_packet& msg)
      {
        return std::time(0) - msg.t > 255;
      }
    )
    , std::end(g_sent_packets)
  );
}

void process_packet(const u_char* buffer, int buffer_size)
{
  if (!is_chat_add_packet(buffer, buffer_size))
  {
    return;
  }

  remove_old_sent_packets();
  if (is_duplicate_packet(buffer, buffer_size))
  {
    BOOST_LOG_TRIVIAL(info) << "**************** DUPLICATE PACKET!!! ****************" << '\n';
    return;
  }
  add_packet_to_sent_packets(buffer, buffer_size);

  const message& cur_msg = get_message(buffer, buffer_size);

  try
  {
    boost::filesystem::create_directory(g_rust_server_ip_address);
  }
  catch (const boost::filesystem::filesystem_error& ex)
  {
    BOOST_LOG_TRIVIAL(error) << "An error occurred while creating directory \'" << g_rust_server_ip_address
      << "\': " << ex.what();
    return;
  }
  
  boost::filesystem::path output_file_path(
    g_rust_server_ip_address + "/" + koicxx::time::get_cur_date("%d.%m.%y") + ".txt"
  );
  boost::filesystem::ofstream f(output_file_path, std::ios_base::app);
  f << '[' << koicxx::time::get_cur_time() << "] " << cur_msg.sender << ": " << cur_msg.msg << '\n';

  const data_payload packet_data_payload = get_udp_data_payload(buffer, buffer_size);
  BOOST_LOG_TRIVIAL(info) << "chat_add packet received. "
                          << "hex: " << get_hex_representation_of_data(packet_data_payload.data, packet_data_payload.data_size)
                          << " | sender: " << cur_msg.sender << " | message: " << cur_msg.msg;
}

boost::property_tree::ptree get_config(boost::filesystem::path config_path)
{
  boost::property_tree::ptree config;

  try
  {
    boost::property_tree::read_ini(config_path.string(), config);
  }
  catch (const boost::property_tree::ini_parser_error& ex)
  {
    BOOST_LOG_TRIVIAL(error) << "An error occurred while reading config file: " << ex.what();
  }

  return config;
}

std::string get_rust_server_ip_address(boost::property_tree::ptree config)
{
  std::string rust_server_ip_address;

  try
  {
    rust_server_ip_address = config.get<std::string>("rust_server.ip_address");
  }
  catch (const boost::property_tree::ptree_error& ex)
  {
    BOOST_LOG_TRIVIAL(error) << "An error occurred while getting Rust server IP address from config: " << ex.what();
  }

  return rust_server_ip_address;
}

void init_logger()
{
  boost::log::add_console_log(std::clog, boost::log::keywords::format = "%Message%");

  boost::filesystem::create_directory("logs");
  boost::log::add_file_log
  (
    boost::log::keywords::file_name = "logs/%Y-%m-%d_%H-%M-%S.%N.log",
    boost::log::keywords::rotation_size = 10 * 1024 * 1024,
    boost::log::keywords::time_based_rotation = boost::log::sinks::file::rotation_at_time_point(0, 0, 0),
    boost::log::keywords::format = "[%TimeStamp%]: %Message%",
    boost::log::keywords::auto_flush = true
  );
  boost::log::add_common_attributes();
}

int main()
{
  init_logger();

  BOOST_LOG_TRIVIAL(info) << "Rust Chats Saver v1.0.0";
  BOOST_LOG_TRIVIAL(info) << "Started at " << koicxx::time::get_cur_time();
  BOOST_LOG_TRIVIAL(info) << "========================================";

  boost::property_tree::ptree config = get_config("config.ini");
  if (config.empty())
  {
    return EXIT_FAILURE;
  }
  g_rust_server_ip_address = get_rust_server_ip_address(config);
  if (g_rust_server_ip_address.empty())
  {
    return EXIT_FAILURE;
  }
  BOOST_LOG_TRIVIAL(info) << "Rust server IP address: " << g_rust_server_ip_address;
  BOOST_LOG_TRIVIAL(info) << "========================================";

  char errbuf[PCAP_ERRBUF_SIZE];

  pcap_if_t* alldevs;
  if (pcap_findalldevs_ex(PCAP_SRC_IF_STRING, NULL, &alldevs, errbuf) == -1)
  {
    BOOST_LOG_TRIVIAL(error) << "An error occurred while using function pcap_findalldevs_ex: " << errbuf;
    return EXIT_FAILURE;
  }
  BOOST_SCOPE_EXIT_ALL(alldevs)
  {
    pcap_freealldevs(alldevs);
  };

  std::cout << "Network interfaces: \n\n";
  pcap_if_t* d;
  int i = 0;
  for (d = alldevs; d; d = d->next)
  {
    std::cout << '[' << ++i << "] " << d->name << '\n';
    if (d->description)
    {
      std::cout << '(' << d->description << ')';
    }
    else
    {
      std::cout << "(No description available)";
    }
    std::cout << '\n';
  }
  std::cout << '\n';

  std::cout << "Enter the interface number you would like to sniff: ";
  int interface_number;
  std::cin >> interface_number;
  if (!std::cin || interface_number > i)
  {
    BOOST_LOG_TRIVIAL(error) << "Invalid interface number";
    return EXIT_FAILURE;
  }

  // Jump to the selected adapter
  for (d = alldevs, i = 0; i < interface_number - 1; d = d->next, ++i);
  BOOST_LOG_TRIVIAL(info) << "Interface: " << d->name;

  if (std::strlen(d->name) > PCAP_BUF_SIZE)
  {
    BOOST_LOG_TRIVIAL(error) << "The name of the interface is greater than the max. allowed size";
    return EXIT_FAILURE;
  }

  pcap_t* fp = pcap_open(
    d->name,                    // name of the device
    MAX_IP_PACKET_SIZE,         // portion of the packet to capture. 
                                // 65536 guarantees that the whole packet will be captured on all the link layers
    PCAP_OPENFLAG_PROMISCUOUS,  // promiscuous mode
    PACKET_READ_TIMEOUT,        // read timeout
    NULL,                       // authentication on the remote machine
    errbuf                      // error buffer
  );
  if (fp == NULL)
  {
    BOOST_LOG_TRIVIAL(error) << "An error occurred while using function pcap_open: " << errbuf;
    return EXIT_FAILURE;
  }

  bpf_u_int32 netmask;
  if (d->addresses != NULL)
  {
    netmask = ((struct sockaddr_in *)(d->addresses->netmask))->sin_addr.S_un.S_addr;
  }
  else
  {
    // If the interface is without an address we suppose to be in a C class network
    netmask = 0xffffff;
  }

  bpf_program fcode;
  if (pcap_compile(fp, &fcode, ("udp and src host " + g_rust_server_ip_address).c_str(), 1, netmask) == -1)
  {
    BOOST_LOG_TRIVIAL(error) << "An error occurred while using function pcap_compile: " << pcap_geterr(fp);
    return EXIT_FAILURE;
  }

  if (pcap_setfilter(fp, &fcode) == -1)
  {
    BOOST_LOG_TRIVIAL(error) << "An error occurred while using function pcap_setfilter: " << pcap_geterr(fp);
    return EXIT_FAILURE;
  }

  BOOST_LOG_TRIVIAL(info) << "========================================";

  BOOST_LOG_TRIVIAL(info) << "Sniffing started";
  pcap_pkthdr* header;
  const u_char* pkt_data;
  u_int res;
  while ((res = pcap_next_ex(fp, &header, &pkt_data)) >= 0)
  {
    if (res == 0)
    {
      // Timeout elapsed
      continue;
    }

    process_packet(pkt_data, header->caplen);
  }
  if (res == -1)
  {
    BOOST_LOG_TRIVIAL(error) << "An error occurred while using function pcap_next_ex";
    return EXIT_FAILURE;
  }
}
